# Dask via HTCondor Test

Example of running a parallelized dask computation on an HTCondor cluster.

## Instructions

```shell
pip install -e .
test.py
```

## Useful References
- [dask-jobqueue](https://jobqueue.dask.org/en/latest/index.html)
- [dask-jobqueue.HTCondorCluster](https://jobqueue.dask.org/en/latest/generated/dask_jobqueue.HTCondorCluster.html)