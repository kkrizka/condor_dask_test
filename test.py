#!/usr/bin/env python

#
# Create the dask cluster
from dask_jobqueue import HTCondorCluster

cluster=HTCondorCluster(
    cores=1, memory="4GB", disk="1GB",
    log_directory='logs/'
)
cluster.scale(10)

#
# Connect to cluster via client
from dask.distributed import Client

client = Client(cluster)

# Local test
# client = Client(processes=False, threads_per_worker=4,
#                 n_workers=1, memory_limit='2GB')

#
# Test some fun!
import dask.array as da
x = da.random.random((10000, 10000), chunks=(1000, 1000))
y = x + x.T
z = y[::2, 5000:].mean(axis=1)
print(z.compute())
